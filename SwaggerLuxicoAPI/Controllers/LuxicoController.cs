﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LuxicoApi.Controllers
{
    [Route("api/luxico")]
    [ApiController]
    public class LuxicoController : ControllerBase
    {
        [HttpPost]
        public IActionResult Index([FromBody] Basic obj)
        {
            return new JsonResult(new { Id = obj.Id });
        }
    }

    public class Basic
    {
        public int Id { set; get; }
        public string Name { set; get; }
    }
}