﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LuxicoApi.Controllers
{
    [Route("api/luxico")]
    [ApiController]
    [Authorize]
    public class LuxicoController : ControllerBase
    {
        [HttpPost]
        public IActionResult Index([FromBody] Basic obj)
        {
            return new JsonResult(new { Id = "1" });
        }
    }

    public class Basic
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }
}